package dev.delle.tim.semanticweb.util;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by lyco on 19/06/15.
 */
public class HTTPUtil {

    //public static final String URL_PLAYGROUNDS = "https://raw.githubusercontent.com/CodeforLeipzig/kidsle/master/project/playgrounds/data/new_playgrounds.json";
    public static final String URL_PLAYGROUNDS = "https://gitlab.com/maokai/public/raw/master/playgrounds.json";
    public static final String URL_PLAYGROUNDS_SMALL = "https://gitlab.com/maokai/public/raw/master/playgrounds_small.json";

    public static final String URL_SCHOOLS = "https://gitlab.com/maokai/public/raw/master/schools2.json";
    public static final String URL_SCHOOLS_SMALL = "https://gitlab.com/maokai/public/raw/master/schools2.json";

    public static final String URL_HOTELS = "https://gitlab.com/maokai/public/raw/master/hotels.json";
    public static final String URL_DISTANCES = "http://router.project-osrm.org/table?";
    public static final String FILE_SCHOOLS = "schools2.json";

    public static String sendGet(String pUrl) throws Exception {

        URL url = new URL(pUrl);

        HttpURLConnection connection = (HttpURLConnection)url.openConnection();
        connection.setRequestMethod("GET");
        connection.setRequestProperty("User-Agent", "Mozilla/5.0");

        int responseCode = connection.getResponseCode();
        if(responseCode == 200){
            //System.out.println("Success code - " + responseCode);

            BufferedReader input = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String line;
            StringBuffer response = new StringBuffer();

            while((line = input.readLine()) != null){
                response.append(line);
            }
            input.close();
            return response.toString();

        }else{
            System.out.println("Error code - "+  responseCode);
            return null;
        }
    }

    public static String sendGet(String pUrl, float lat1, float long1, float lat2, float long2) throws Exception {

        URL url = new URL(pUrl + "loc=" + String.valueOf(lat1) + "," + String.valueOf(long1) + "&loc=" +
                String.valueOf(lat2) + "," + String.valueOf(long2));

        HttpURLConnection connection = (HttpURLConnection)url.openConnection();
        connection.setRequestMethod("GET");
        connection.setRequestProperty("User-Agent", "Mozilla/5.0");

        int responseCode = connection.getResponseCode();
        if(responseCode == 200){

            BufferedReader input = new BufferedReader(new InputStreamReader(connection.getInputStream()));

            String line;
            StringBuffer response = new StringBuffer();
            while((line = input.readLine()) != null){
                response.append(line);
            }
            input.close();
            return response.toString();

        }else{
            System.out.println("Error code - "+  responseCode);
            return null;
        }
    }
}
