package dev.delle.tim.semanticweb.data.schools;

/**
 * Created by lyco on 31/05/15.
 */
public class Schools {
    private Schule[] schools;

    public Schools(Schule[] schools) {
        this.schools = schools;
    }

    public Schule[] getSchools() {
        return schools;
    }

    public void setSchools(Schule[] schools) {
        this.schools = schools;
    }
}
