package dev.delle.tim.semanticweb.data.playgrounds;

import dev.delle.tim.semanticweb.data.hotels.Elements;
import dev.delle.tim.semanticweb.data.schools.Schule;

import java.util.List;

/**
 * Created by lyco on 30/05/15.
 */
public class Playground {
    private General general;
    private Advanced advanced;
    private List<Schule> schools;
    private List<Elements> hotels;

    public Playground(General genera, Advanced advanced) {
        this.general = genera;
        this.advanced = advanced;
    }

    public General getGeneral() {
        return general;
    }

    public void setGeneral(General genera) {
        this.general = general;
    }

    public Advanced getAdvanced() {
        return advanced;
    }

    public void setAdvanced(Advanced advanced) {
        this.advanced = advanced;
    }

    public List<Schule> getSchools() {
        return schools;
    }

    public void setSchools(List<Schule> schools) {
        this.schools = schools;
    }

    public List<Elements> getHotels() {
        return hotels;
    }

    public void setHotels(List<Elements> hotels) {
        this.hotels = hotels;
    }
}
