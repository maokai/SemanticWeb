package dev.delle.tim.semanticweb.data.playgrounds;

/**
 * Created by lyco on 30/05/15.
 */
public class Advanced {
    private String[] equipment;
    private String[] gaming_devices;

    public Advanced(String[] equipment, String[] gaming_devices) {
        this.equipment = equipment;
        this.gaming_devices = gaming_devices;
    }

    public String[] getEquipment() {
        return equipment;
    }

    public void setEquipment(String[] equipment) {
        this.equipment = equipment;
    }

    public String[] getGaming_devices() {
        return gaming_devices;
    }

    public void setGaming_devices(String[] gaming_devices) {
        this.gaming_devices = gaming_devices;
    }
}
