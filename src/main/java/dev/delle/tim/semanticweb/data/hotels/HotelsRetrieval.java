package dev.delle.tim.semanticweb.data.hotels;

import com.google.gson.Gson;
import dev.delle.tim.semanticweb.util.HTTPUtil;

/**
 * Created by lyco on 19/06/15.
 */
public class HotelsRetrieval {
    String hotels;
    Hotels hotels_list;

    public HotelsRetrieval(String pUrl) throws Exception {

        this.hotels = HTTPUtil.sendGet(pUrl);

        System.out.println(this.hotels);

        this.hotels_list = new Gson().fromJson(this.hotels, Hotels.class);

        for (Elements element : hotels_list.getElements()) {
            System.out.println(element.getTags().getName());
        }
    }

    public String getHotels() {
        return hotels;
    }

    public void setHotels(String hotels) {
        this.hotels = hotels;
    }

    public Hotels getHotels_list() {
        return hotels_list;
    }

    public void setHotels_list(Hotels hotels_list) {
        this.hotels_list = hotels_list;
    }
}
