package dev.delle.tim.semanticweb.data.playgrounds;

/**
 * Created by lyco on 30/05/15.
 */
public class General {
    private String district;
    private float latitude;
    private String lines;
    private String location;
    private float longitude;
    private String name;
    private String stops;
    private String town;

    public General(String district, float latitude, String lines, String location, float longitude, String name, String stops, String town) {
        this.district = district;
        this.latitude = latitude;
        this.lines = lines;
        this.location = location;
        this.longitude = longitude;
        this.name = name;
        this.stops = stops;
        this.town = town;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public float getLatitude() {
        return latitude;
    }

    public void setLatitude(float latitude) {
        this.latitude = latitude;
    }

    public String getLines() {
        return lines;
    }

    public void setLines(String lines) {
        this.lines = lines;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public float getLogtitude() {
        return longitude;
    }

    public void setLogtitude(float logtitude) {
        this.longitude = logtitude;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStops() {
        return stops;
    }

    public void setStops(String stops) {
        this.stops = stops;
    }

    public String getTown() {
        return town;
    }

    public void setTown(String town) {
        this.town = town;
    }
}
