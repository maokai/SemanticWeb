package dev.delle.tim.semanticweb.data.schools;

import com.google.gson.Gson;
import dev.delle.tim.semanticweb.util.HTTPUtil;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * Created by lyco on 31/05/15.
 */
public class SchoolsRetrieval {

    private String schools;
    private Schools school_list;

    public SchoolsRetrieval(String pUrl) throws Exception{
        //this.schools = readFile(pFilePath);

        this.schools = HTTPUtil.sendGet(pUrl);

        System.out.println(this.schools);

        this.school_list = new Schools(new Gson().fromJson(this.schools, Schule[].class));

        for(Schule schule : this.school_list.getSchools()){
            System.out.println(schule.getSchool().getValue() + " --> " + schule.getAddress().getValue() + " Lat: " +
                    String.valueOf(schule.getLat().getValue()) + " Long: " + schule.getLong().getValue());
        }

    }

    private String readFile(String pFilePath) throws IOException {

        byte[] encoded = Files.readAllBytes(Paths.get(pFilePath));

        return new String(encoded, Charset.defaultCharset());
    }

    public String getSchools() {
        return schools;
    }

    public void setSchools(String schools) {
        this.schools = schools;
    }

    public Schools getSchool_list() {
        return school_list;
    }

    public void setSchool_list(Schools school_list) {
        this.school_list = school_list;
    }
}
