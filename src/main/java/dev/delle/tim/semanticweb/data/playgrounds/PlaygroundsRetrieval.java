package dev.delle.tim.semanticweb.data.playgrounds;

import com.google.gson.Gson;
import dev.delle.tim.semanticweb.data.hotels.Hotels;
import dev.delle.tim.semanticweb.data.schools.Schools;
import dev.delle.tim.semanticweb.util.HTTPUtil;

import java.io.BufferedReader;
import java.io.FileReader;

/**
 * Created by lyco on 30/05/15.
 */
public class PlaygroundsRetrieval {

    private String playgrounds;
    private Playgrounds playgrounds_list;
    private Hotels hotels;
    private Schools schools;


    public PlaygroundsRetrieval(String pUrl) throws Exception{

        //----------------------------------
        // GET PLAYGROUND DATA
        //----------------------------------

        this.playgrounds = HTTPUtil.sendGet(pUrl);

        Gson gson = new Gson();

        this.playgrounds_list = new Playgrounds(gson.fromJson(this.playgrounds, Playground[].class));

        for(Playground playground: playgrounds_list.getPlaygrounds()){
            System.out.println(playground.getGeneral().getName());

            for(String string : playground.getAdvanced().getEquipment()){
                System.out.println(string);
            }

            for(String string : playground.getAdvanced().getGaming_devices()){
                System.out.println(string);
            }

        }

    }

    //----------------------------------
    // GETTER
    //----------------------------------

    public String getPlaygrounds() {
        return playgrounds;
    }

    public Playgrounds getPlaygrounds_list() {
        return playgrounds_list;
    }

    public void setPlaygrounds_list(Playgrounds playgrounds_list) {
        this.playgrounds_list = playgrounds_list;
    }

    public void setPlaygrounds(String playgrounds) {
        this.playgrounds = playgrounds;
    }

    public Hotels getHotels() {
        return hotels;
    }

    public void setHotels(Hotels hotels) {
        this.hotels = hotels;
    }

    public Schools getSchools() {
        return schools;
    }

    public void setSchools(Schools schools) {
        this.schools = schools;
    }
}
