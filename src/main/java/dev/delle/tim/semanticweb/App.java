package dev.delle.tim.semanticweb;

import com.google.gson.Gson;
import com.hp.hpl.jena.ontology.*;
import com.hp.hpl.jena.query.Dataset;
import com.hp.hpl.jena.rdf.model.*;
import com.hp.hpl.jena.tdb.TDBFactory;
import com.hp.hpl.jena.tdb.TDBLoader;
import com.hp.hpl.jena.vocabulary.RDF;
import dev.delle.tim.semanticweb.data.hotels.Elements;
import dev.delle.tim.semanticweb.data.hotels.HotelsRetrieval;
import dev.delle.tim.semanticweb.data.playgrounds.DistanceTable;
import dev.delle.tim.semanticweb.data.playgrounds.Playground;
import dev.delle.tim.semanticweb.data.playgrounds.Playgrounds;
import dev.delle.tim.semanticweb.data.playgrounds.PlaygroundsRetrieval;
import dev.delle.tim.semanticweb.data.schools.SchoolsRetrieval;
import dev.delle.tim.semanticweb.data.schools.Schule;
import dev.delle.tim.semanticweb.util.HTTPUtil;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class App 
{


    private static final String BASE = "http://tim.delle.org/playground/";
    private static Playground[] playgrounds;
    private static Schule[] schools;


    public static void main( String[] args ) {

        try {

            //----------------------------------
            // CHECK LOCAL FILE
            //----------------------------------
            File file = new File("playgrounds.json");

            if(file.exists()){
                BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream("playgrounds.json"), "UTF8"));
                playgrounds = new Gson().fromJson(reader, Playground[].class);

                System.out.println("Playgrounds loaded!\n");

            }else {

                //----------------------------------
                // GET DATA
                //----------------------------------
                PlaygroundsRetrieval playgroundsRetrieval = new PlaygroundsRetrieval(HTTPUtil.URL_PLAYGROUNDS);
                SchoolsRetrieval schoolsRetrieval = new SchoolsRetrieval(HTTPUtil.URL_SCHOOLS);
                HotelsRetrieval hotelsRetrieval = new HotelsRetrieval(HTTPUtil.URL_HOTELS);

                //----------------------------------
                // GET DISTANCES SCHOOLS
                //----------------------------------
                playgrounds = playgroundsRetrieval.getPlaygrounds_list().getPlaygrounds();
                schools = schoolsRetrieval.getSchool_list().getSchools();
                List<Elements> hotels = hotelsRetrieval.getHotels_list().getElements();


                String distance_matrix;
                DistanceTable distanceTable;
                Gson gson = new Gson();
                ArrayList<Double> distances;

                for (int i = 0; i < playgrounds.length; i++) {

                    List<Schule> playgroundSchools = new ArrayList<Schule>();

                    float lat1 = playgrounds[i].getGeneral().getLatitude();
                    float long1 = playgrounds[i].getGeneral().getLogtitude();

                    for (int j = 0; j < schools.length; j++) {

                        float lat2 = Float.parseFloat(schools[j].getLat().getValue());
                        float long2 = Float.parseFloat(schools[j].getLong().getValue());

                        //System.out.println("Evaluate coordinates:\n" + lat1 + ":" + long1 + " --> " + lat2 + ":" + long2);

                        distance_matrix = HTTPUtil.sendGet(HTTPUtil.URL_DISTANCES, lat1, long1, lat2, long2);
                        distanceTable = gson.fromJson(distance_matrix, DistanceTable.class);

                        distances = (ArrayList<Double>) distanceTable.getDistance_table().get(0);
                        double distance = distances.get(1);

                        if (distance < 500.0d) {
                            System.out.println("Distance: " + distance);

                            playgroundSchools.add(schools[j]);

                        }

                    }

                    if (playgroundSchools.size() > 0) {
                        playgrounds[i].setSchools(playgroundSchools);
                        System.out.println("Schools added to Playground: " + playgrounds[i].getGeneral().getName());
                    }

                }

                System.out.println("School distances evaluated!");

                //----------------------------------
                // GET DISTANCES HOTELS
                //----------------------------------

                for (int i = 0; i < playgrounds.length; i++) {

                    List<Elements> playgroundHotels = new ArrayList<Elements>();

                    float lat1 = playgrounds[i].getGeneral().getLatitude();
                    float long1 = playgrounds[i].getGeneral().getLogtitude();

                    for (int j = 0; j < hotels.size(); j++) {

                        float lat2 = Float.parseFloat(String.valueOf(hotels.get(j).getLat()));
                        float long2 = Float.parseFloat(String.valueOf(hotels.get(j).getLon()));
                        //System.out.println("Evaluate coordinates:\n" + lat1 + ":" + long1 + " --> " + lat2 + ":" + long2);

                        distance_matrix = HTTPUtil.sendGet(HTTPUtil.URL_DISTANCES, lat1, long1, lat2, long2);
                        distanceTable = gson.fromJson(distance_matrix, DistanceTable.class);

                        distances = (ArrayList<Double>) distanceTable.getDistance_table().get(0);
                        double distance = distances.get(1);

                        if (distance < 1000.0d) {
                            System.out.println("Distance: " + distance);

                            playgroundHotels.add(hotels.get(j));

                        }

                    }

                    if (playgroundHotels.size() > 0) {
                        playgrounds[i].setHotels(playgroundHotels);
                        System.out.println("Hotels added to Playground: " + playgrounds[i].getGeneral().getName());
                    }

                }


                //----------------------------------
                // SAVE JSON TO FILE
                //---------------------------------
                String playgroundsString = gson.toJson(playgrounds);

                BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("playground-model"), "UTF8"));
                bufferedWriter.write(playgroundsString);
                bufferedWriter.flush();
                bufferedWriter.close();
                System.out.println("Playgrounds saved!");

            }

            //----------------------------------
            // GENERATE RDF SCHEMA EXAMPLE
            //----------------------------------

            Model model = ModelFactory.createDefaultModel();

            // Ontology
            OntModel ontModel = ModelFactory.createOntologyModel(OntModelSpec.OWL_DL_MEM_RULE_INF);

            //----------------------------------
            // GENERATE RDF SCHEMA CLASSES
            //----------------------------------
            OntClass playgroundClass = ontModel.createClass(BASE + "playground");
            OntClass schoolClass = ontModel.createClass(BASE + "school");
            OntClass hotelClass = ontModel.createClass(BASE + "hotel");

            //----------------------------------
            // GENERATE CLASSES PROPERTIES
            //----------------------------------
            playgroundClass.addLabel("playground in Leipzig", "en");
            schoolClass.addLabel("school in Leipzig", "en");
            hotelClass.addLabel("hotel in Leipzig", "en");

            DatatypeProperty nameProperty = ontModel.createDatatypeProperty(BASE + "name");
            DatatypeProperty latitudeProperty = ontModel.createDatatypeProperty(BASE + "latitude");
            DatatypeProperty longitudeProperty = ontModel.createDatatypeProperty(BASE + "longitude");
            DatatypeProperty hasEquipmentProperty = ontModel.createDatatypeProperty(BASE + "hasEquipment");
            DatatypeProperty hasGamingDeviceProperty = ontModel.createDatatypeProperty(BASE + "hasGamingDevice");
            DatatypeProperty hasAddressProperty = ontModel.createDatatypeProperty(BASE + "address");



            DatatypeProperty hasHotel = ontModel.createDatatypeProperty(BASE + "hasHotel");
            hasHotel.addDomain(schoolClass);

            DatatypeProperty hasSchool = ontModel.createDatatypeProperty(BASE + "hasSchool");
            hasSchool.addDomain(hotelClass);

            for(Playground playground : playgrounds){


                String name = playground.getGeneral().getName().replaceAll("\\s+","").replaceAll("&quot;", "");
                Resource subject = model.createResource(BASE + name);

                // ontology approach
                Individual playgr = ontModel.createIndividual(BASE + name, playgroundClass);
                playgr.addProperty(latitudeProperty, String.valueOf(playground.getGeneral().getLatitude()));
                playgr.addProperty(longitudeProperty, String.valueOf(playground.getGeneral().getLogtitude()));


                subject.addProperty(createProperty("hasLatitude"), String.valueOf(playground.getGeneral().getLatitude()));
                subject.addProperty(createProperty("hasLongitude"), String.valueOf(playground.getGeneral().getLogtitude()));


                for(String equipment : playground.getAdvanced().getEquipment()){

                    // ontology approach
                    playgr.addProperty(hasEquipmentProperty, equipment);

                    subject.addProperty(createProperty("hasEquipment"), equipment);
                }

                for(String gamingdevice : playground.getAdvanced().getGaming_devices()){

                    // ontology approach
                    playgr.addProperty(hasGamingDeviceProperty, gamingdevice);

                    subject.addProperty(createProperty("hasGamingDevice"), gamingdevice);
                }

                if(playground.getSchools() != null){
                    for(Schule schule : playground.getSchools()){

                        // ontology approach
                        Individual school = ontModel.createIndividual(BASE + schule.getLabel().getValue().replaceAll("\\s+", ""), schoolClass);
                        school.addProperty(hasAddressProperty, schule.getAddress().getValue().replaceAll("\\s+", ""));
                        school.addProperty(latitudeProperty ,schule.getLat().getValue());
                        school.addProperty(longitudeProperty ,schule.getLong().getValue());
                        playgr.addProperty(hasSchool, school);

                        model.add(subject, createProperty("hasSchool"), model.createResource(BASE + schule.getLabel().
                                        getValue().replaceAll("\\s+", ""))
                                .addProperty(createProperty("hasAddress"), schule.getAddress().getValue().replaceAll("\\s+", ""))
                                .addProperty(createProperty("hasLatitude"), schule.getLat().getValue())
                                .addProperty(createProperty("hasLongitude"), schule.getLong().getValue())
                        );
                    }
                }

                if(playground.getHotels() != null){
                    for(Elements hotel : playground.getHotels()){

                        // ontology approach
                        Individual hotelInstance = ontModel.createIndividual(BASE + hotel.getTags().getName()
                                .replaceAll("\\s+", ""), hotelClass);
                        hotelInstance.addProperty(hasAddressProperty, hotel.getTags().getAddrCity() + ", " + hotel.getTags().getAddrPostcode());
                        hotelInstance.addProperty(latitudeProperty ,String.valueOf(hotel.getLat()));
                        hotelInstance.addProperty(longitudeProperty ,String.valueOf(hotel.getLon()));
                        playgr.addProperty(hasHotel, hotelInstance);

                        model.add(subject, createProperty("hasHotel"), model.createResource(BASE+hotel.getTags().getName()
                                .replaceAll("\\s+", ""))
                                .addProperty(createProperty("hasAddress"), hotel.getTags().getAddrCity() + ", " + hotel.getTags().getAddrPostcode())
                                .addProperty(createProperty("hasLatitude"), String.valueOf(hotel.getLat()))
                                .addProperty(createProperty("hasLongitude"), String.valueOf(hotel.getLon())));
                    }
                }

                model.setNsPrefix("pl", BASE);
                model.setNsPrefix("xsd", "http://www.w3.org/2001/XMLSchema#");

                ontModel.setNsPrefix("pl", BASE);
                ontModel.write(System.out, "RDF/XML");
                ontModel.write(new BufferedWriter(new OutputStreamWriter(new FileOutputStream("playground-model"), "UTF8")));


                //model.write(System.out, "RDF/XML");
                //model.write(new FileWriter("playground-model"));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private static Property createProperty(String pLocalName){
        return ResourceFactory.createProperty(BASE, pLocalName);
    }

    private static Resource createResource(String pLocalName){
        return ResourceFactory.createResource(BASE+pLocalName);
    }
}
